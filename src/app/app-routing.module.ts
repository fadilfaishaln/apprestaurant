import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrdersComponent } from './orders/orders.component';
import { OrderComponent } from './orders/order/order.component';

const routes: Routes = [
  { path: '', redirectTo: 'Order', pathMatch: 'full' },
  { path: 'Orders', component: OrdersComponent },
  { path: 'Order', children : [
    { path: '', component: OrderComponent },
    { path: 'edit/:id', component: OrderComponent }
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
