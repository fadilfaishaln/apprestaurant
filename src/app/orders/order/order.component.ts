import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { OrderService } from 'src/app/shared/order.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { OrderItemsComponent } from '../order-items/order-items.component';
import { CustomerService } from 'src/app/shared/customer.service';
import { Customer } from 'src/app/shared/customer.model';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styles: []
})
export class OrderComponent implements OnInit {

  ListCustomer: Customer[];
  isValid: boolean = true;

  constructor(
    private service: OrderService,
    private dialog: MatDialog,
    private CustomerSrvc: CustomerService,
    private toastr: ToastrService,
    private router: Router,
    private CRoute: ActivatedRoute ) { }

  ngOnInit() {
    let orderID = this.CRoute.snapshot.paramMap.get('id');
    if ( orderID == null) {
      this.resetForm();
    }
    else
    {
      this.service.getOrderByID(parseInt(orderID)).then( res => {
        this.service.formOrder = res.order;
        this.service.orderItems = res.orderDetails;
      });
    }
    this.CustomerSrvc.getCustomerList().then( res => this.ListCustomer = res as Customer[] );
  }

  resetForm(form?: NgForm) {
    if ( form = null )
      form.resetForm();
    this.service.formOrder = {
      OrderID: null,
      OrderNo: Math.floor(10000 + Math.random() * 999999 ).toString(),
      CustomerID: 0,
      PMethod: '',
      GTotal: 0
    }
    this.service.orderItems = [];
  }

  AddOrEditOrderItem(orderItemIndex ,OrderID) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.width = '50%';
    dialogConfig.data = { orderItemIndex, OrderID };

    this.dialog.open(OrderItemsComponent, dialogConfig).afterClosed().subscribe(res => {
      this.updateGTotal();
    });
  }

  onDeleteItem(OrderItemID: number, i: number) {
    this.service.orderItems.splice(i, 1);
    this.updateGTotal();
  }

  updateGTotal() {
    this.service.formOrder.GTotal =  this.service.orderItems.reduce((prev, curr) =>{
      return prev + curr.Total;
    }, 0 );

    this.service.formOrder.GTotal = parseFloat(this.service.formOrder.GTotal.toFixed(3));
  }

  validateForm() {
    this.isValid = true;
    if (this.service.formOrder.CustomerID == 0) {
      this.isValid = false;
    }
    else if (this.service.orderItems.length == 0) {
      this.isValid = false;
    }
    return this.isValid;
  }

  onSubmit(form: NgForm) {
    if (this.validateForm()) {
      this.service.SaveOrUpdateOrder().subscribe(res => {
        this.resetForm();
        this.toastr.success('Submitted Successfully', 'Restaurant App.');
        this.router.navigate(['/orders']);
      })
    }
  }
}