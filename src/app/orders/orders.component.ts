import { Component, OnInit } from '@angular/core';
import { OrderService } from '../shared/order.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styles: []
})
export class OrdersComponent implements OnInit {

  orderList;
  constructor (
    private OrderSrvc: OrderService,
    private router: Router ) { }

  ngOnInit() {
    this.OrderSrvc.getOrderList().then(res => this.orderList = res);
  }

  openForEdit(orderID: number) {
    this.router.navigate(['Order/edit/' + orderID]);
  }
}
