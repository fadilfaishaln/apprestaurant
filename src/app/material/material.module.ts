import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import * as Material from '@angular/material';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    Material.MatDialogModule,
    Material.MatCardModule,
    Material.MatFormFieldModule,
    Material.MatInputModule,
    Material.MatSelectModule,
    Material.MatDatepickerModule,
    Material.MatNativeDateModule,
    Material.MatRadioModule,
    Material.MatProgressBarModule,
    Material.MatTableModule,
    Material.MatGridListModule,
    Material.MatButtonModule
  ],
  exports: [
    Material.MatDialogModule,
    Material.MatCardModule,
    Material.MatFormFieldModule,
    Material.MatInputModule,
    Material.MatSelectModule,
    Material.MatDatepickerModule,
    Material.MatNativeDateModule,
    Material.MatRadioModule,
    Material.MatProgressBarModule,
    Material.MatTableModule,
    Material.MatGridListModule,
    Material.MatButtonModule
  ]
})
export class MaterialModule { }
